# FoundryVTT Chat Message Builder

> This Project is currently in the Starting phases. While probably nothing will happen, you should not jet use it in your
> Foundry VTT instance!

> An Addon for [Foundry VTT](https://foundryvtt.com/) directed at Gamemasters, Players and maybe Addon developers, to 
> "improve" the building for chat messages.

## Documentation
The API documentation can be found [here](https://dentur.gitlab.io/foundry-chat-message-builder/). (Currently WIP)

## The Problem
I (the Gamemaster) wanted to add a Macro for my players to roll a skill as it was used often. I needed i could not find 
a documented way to use the roll macros, I wrote a script macro which uses the 
[```ChatMessage.create()```](https://foundryvtt.com/api/ChatMessage.html#.create) API. Sadly I could not easily find out 
how to use it and found that I needed to write the message HTML as a String.

> Sidenote: I know I could have used a templating engine, but for a macro that is a bit much to ask (IMO)

## (My) Solution
This addon has one major goal: To provide Macro Authors a way to easily generate good(ish) looking chat messages.
This will be archived by three components:
- An extendable template system, which allows other Addons to register Templates. These Templates will allow 
  macro authors to quickly create a message with a certain look. I would like to use 
  [Bettter Rolls for 5e](https://github.com/RedReign/FoundryVTT-BetterRolls5e) as an example.
  Their rolls contain a header with a name of what was rolled, a character picture, and some additional roll stuff 
  (e.g. advantage). But if one uses the ChatMessage API, all of that has to be added by hand.
- An HTML builder system for most common elements, and some FoundryVTT custom elements (For example the header 
  mentioned in the first point).
- A proper Documentation. To use a API efficiently a good (or at least decent) Documentation. As such i will provide an
  [online documentation](https://dentur.gitlab.io/foundry-chat-message-builder/) and later on possible an "ingame" 
  documentation
