import {ChatBaseElement} from "./ChatBaseElement.js";
import {ChatDivElement} from "./html/ChatDivElement.js";
import {ChatAElement} from "./html/ChatAElement.js";
import {ChatHtmlElement} from "./html/ChatHtmlElement.js";
import {ChatImgElement} from "./html/ChatImgElement.js";
import {ChatPElement} from "./html/ChatPElement.js";
import {ChatSpanElement} from "./html/ChatSpanElement.js";
import {ChatVerticalListElement} from "./predefined/ChatVerticalListElement.js";
import {ChatH1Element} from "./html/ChatH1Element.js";
import {ChatH6Element} from "./html/ChatH6Element.js";
import {ChatH5Element} from "./html/ChatH5Element.js";
import {ChatH4Element} from "./html/ChatH4Element.js";
import {ChatH3Element} from "./html/ChatH3Element.js";
import {ChatH2Element} from "./html/ChatH2Element.js";
import {ChatHeaderElement} from "./predefined/ChatHeaderElement.js";
import {ChatButtonElement} from "./html/ChatButtonElement.js";

/**
 * @external User
 * @see https://foundryvtt.com/api/User.html
 */

/**
 * The data to identify the Speaker
 * @external Speaker
 * @see https://foundryvtt.com/api/ChatMessage.html#.getSpeaker
 */

/**
 * @external Roll
 * @see https://foundryvtt.com/api/Roll.html
 */

/**
 * @external RollMode
 * @typedef {("roll"|"selfrolL"|"gmroll"|"blindroll")}
 */

/**
 * Class that helps create ChatMessages @see https://foundryvtt.com/api/ChatMessage.html
 * @external ChatMessage
 */

/**
 * @method ChatMessage#create
 * @param {ChatMessageCreateArgs} createArgs Options object containing all information to create and send a Chat message in Foundry
 */

/**
 * Parameters used by the [ChatMessage.create()](https://foundryvtt.com/api/ChatMessage.html#.create) method
 * @typedef ChatMessageCreateArgs
 * @type Object
 * @property user {User} The user which will send the message. (Default: Current User)
 * @property speaker {Speaker} The speaker of the message. (Default: ChatMessage.getSpeaker())
 * @property content {String} The chat content. Will be overwritten by the result of the render of ChatMessageBuilder
 * @property rollMode {RollMode} The Mode the roll is rolled in (Default: "roll") {@link RollMode}
 * TODO: Analyse code to find further options
 */


/**
 * The Base element to create a Chat Message.
 * @example <caption>Building a basic message</caption>
 * // Builds a message with a generic header containing the Character Icon of the current user (if available), a title and a roll
 * // and sends it publicly
 * ChatMessageBuilder.new().add([ChatMessageBuilder.header("Strength check"), roll]).buildAndSend();
 */
export class ChatMessageBuilder extends ChatBaseElement{



    /**
     * Creates a new Instance of the ChatMessageBuilder. <code>ChatMessageBuilder.start()</code> can be used alternatively.
     * @param options {{}} Placeholder for future options
     * @constructor
     */
    constructor(options){
        super();
        this.canHaveChildren = true;
    }

    /**
     * The inline Style which will be applied to the chat message container div
     * @type {Object.<string, string>}
     */
    chatMessageContainerStyle = {};

    /**
     * Sets and <b>Overwrites</b> the {@link chatMessageContainerStyle}. Accepts {@link CSSStyleDeclaration} or dictionary objects.
     * See the List of[common CSS properties](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference)
     * @param style {(Object.<string, (number | string)>)} The CSS elements to set. (hyphenCase)
     * @returns {ChatMessageBuilder} Returns itself for further processing
     */
    setStyle(style){
        if (typeof style === "object"){
            this.chatMessageContainerClasses = {};
            this.addStyle(style);
        }
        else console.warn("ChatMessageBuilder | ChatMessageBuilder.setStyle(style) only accepts parameters of type Object.<string, (number | string)> dictionary")
        return this;
    }

    /**
     * Adds the Elements of the style dictionary to the current inline Style.
     * @param style {Object.<string,(string | number)>} The Css elements to set. See the List of[common CSS properties](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference) for an overview of Options.
     * @returns {ChatMessageBuilder} Returns itself for further processing
     */
    addStyle(style){
        if(typeof style !== "object"){
            console.warn("ChatMessageBuilder | ChatMessageBuilder.addStyle(style) style parameter not of type object. Skipping instruction.");
            return this;
        }
        for(let key in style){
            if(!style.hasOwnProperty(key)) continue;
            if(typeof style[key] === "string")
                this.chatMessageContainerStyle[key] = style[key];
            else
                this.chatMessageContainerStyle[key] = style[key].toString()
        }
        return this;
    }

    /**
     * Array of classes, that are applied to the chat message div
     * @type {string[]}
     */
    chatMessageContainerClasses = ["chat-card"]

    /**
     * Sets and <b>Overwrites</b> the{@link chatMessageContainerClasses} array
     * @param classes {string[]} The new array of classes that will be applied to the chat message div
     * @returns {ChatMessageBuilder} Returns itself for further processing
     */
    setClasses(classes){
        if(Array.isArray(classes)){
            this.chatMessageContainerClasses = classes;
        }
        else{
            console.warn("ChatMessageBuilder | ChatMessageBuilder.setClasses(classes) classes parameter was not an array. Skipping instruction");
        }
        return this;
    }

    /**
     * Adds the class or the classes to the list of {@link chatMessageContainerClasses} that will be applied to the chat message div
     * @param classes {(string | string[])} String or String array of classes that will be added
     * @returns {ChatMessageBuilder}
     */
    addClass(classes){
        if(typeof classes === "string"){
            this.chatMessageContainerClasses.push(classes);
        }
        else if (Array.isArray(classes)){
            for(let classString of classes){
                if(typeof classString === "string") {
                    this.chatMessageContainerClasses.push(classString);
                }
                else {
                    console.warn(`ChatMessageBuilder | ChatMessageBuilder.addClasses(classes) array element ${JSON.stringify(classString)} is no string. Skipping Element.`);
                }
            }
        }
        else{
            console.warn("ChatMessageBuilder | ChatMessageBuilder.addClasses(classes) classes parameter is no string nor an array. Skipping instruction");
        }
        return this;
    }


    /**
     * Renders the chat message including all of the children and Returns a HTML String
     * @returns {Promise<string>} The HTML String representation of the chat message
     */
    async render(){
        let containerNode = document.createElement("div");
        let chatMessage = document.createElement("div")
        for(let styleKey in this.chatMessageContainerStyle){
            if(!this.chatMessageContainerStyle.hasOwnProperty(styleKey)) continue;
            chatMessage.style.setProperty(styleKey, this.chatMessageContainerStyle[styleKey]);
        }
        for(let classString of this.chatMessageContainerClasses){
            chatMessage.classList.add(classString)
        }
        containerNode.appendChild(chatMessage);
        chatMessage.innerHTML = await super.render();

        return containerNode.innerHTML;
    }

    /**
     * Renders the message and sends it. Additional options can be specified by the createMessageOptions parameter.
     * @param createMessageOptions {ChatMessageCreateArgs} Options that will be forwareded to the [ChatMessage.create]() function
     */
    async renderAndSend(createMessageOptions){
        ChatMessage.create({
            ...createMessageOptions,
            content: await this.render()
        })
    }

    /**
     * Creates a new instance of the ChatMessageBuilder. Can be used instead of <code>new ChatMessageBuilder(options)</code>
     * @param options {{}} Placeholder for future options
     * @example
     * // Creates and renders a basic Chat Message
     * ChatMessageBuilder.start().add("Rocks Fall, everyone dies").send();
     */
    static start(options){
        return new ChatMessageBuilder(options);
    }

    /*=================================================================================================
    * ==================================== HTML Elements ==============================================
    * =================================================================================================*/

    /**
     * Creates a new a with the specified url and children. A Elements are clickable links, they can also be used as a parent for other content like images
     * @param url {string} The Url this a element links to
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} The children of this a element, often a string or an Img element
     * @returns {ChatAElement} Returns the a Element for further processing
     * @example
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.a("https://www.example.com", "Hello World"), //Creates a "normal" link
     *      ChatMessageBuilder.a("https://www.example.com", ChatMessageBuilder.img("link/to/the/image/or/svg", 32, 32)) //Creates a clickable Image
     * ]).renderAndSend();
     */
    static a(url, children){
        return new ChatAElement(url, children)
    }

    /**
     * Creates a new button with the specified callback and children. Buttons can be used to interact with the users.
     * <b>Note: By the way chat elements are currently generated the callback function can only access the global scope! Check the examples to see how data can be accessed in the callback</b>
     * @param callback {function(HTMLElement, Object)} The Callback which will be called, when the button is clicked.
     * The first parameter will be the HTMLElement of the Button, the second, the data specified in callbackData
     * <b>Note: As the callback may be executed by a different user (or device) than where the CharMessage were send
     * from, it needs to be fully independent. This manly means, that all data used by the callback needs to
     * be serialized and attached to the message, so that it can be stored on the server. Scoped variables will not Work!
     * Global variables will only work on the device, the chat message was generated on, and will not be persisted
     * between sessions </b>
     * @param callbackData {Object} An serializable object. This data will be
     * serialized and attached to the button. Once clicked, the data will be parsed and handed to the callback.
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} Optional array of children of this element.
     * @return {ChatButtonElement} The Button element
     * @example
     * //Basic usage
     * ChatMessageBuilder.start().add([
     *  ChatMessageBuilder.button((button, unusedData)=>{console.log("Button was clicked")}, {}, "Click Me")
     * ]).renderAndSend();
     *
     * //This does not Work! (The callback cannot access the outer scope!)
     * let roll = new Roll("1d20");
     * ChatMessageBuilder.start().add([
     *  ChatMessageBuilder.button((button)=>{
     *      roll.evaluate();
     *      replaceButtonWithRoll(roll)
 *      }, "Click Me")
     * ]).renderAndSend();
     *
     * //Use this (or something similar) instead
     * let roll = new Roll("1d20");
     * let data = {
     *     roll: roll.toJSON()
     * }
     * ChatMessageBuilder.start().add([
     *  ChatMessageBuilder.button((button, data)=>{
     *      let roll = Roll.fromJSON(data.roll)
     *      roll.evaluate();
     *      replaceButtonWithRoll(roll)
     *      }, data, "Click Me")
     * ]).renderAndSend();
     */
    static button(callback, callbackData, children){
        return new ChatButtonElement(callback, callbackData, children)
    }

    /**
     * Creates a new div with the specified Children. Divs are generic containers, that can be filled with any content
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} A Child or an array of children.
     * @returns {ChatDivElement} Returns the div for further processing
     * @example
     * //Note: It is highly recommended to create a shortcut for ChatMessage Builder as it will drastically shorten code
     * //      For example let cmb = ChatMessageBuilder
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.div("Hello World"),
     *      ChatMessageBuilder.div(predefinedRoll).addClass("MyCustomRollContainerClass")
     * ]).renderAndSend();
     */
    static div(children){
        return new ChatDivElement(children);
    }

    /**
     * Creates a h1 element. h elements are used as headings and range from h1 to h6 where the size of h1 is the biggest
     * and h6 is the smallest
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} A Child or an array of children.
     * @return {ChatH1Element}Returns the h1 for further processing
     * @example
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.h1("Hello World")
     * ]).renderAndSend();
     */
    static h1(children){
        return new ChatH1Element(children);
    }

    /**
     * Creates a h2 element. h elements are used as headings and range from h1 to h6 where the size of h1 is the biggest
     * and h6 is the smallest
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} A Child or an array of children.
     * @return {ChatH2Element}Returns the h2 for further processing
     * @example
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.h2("Hello World")
     * ]).renderAndSend();
     */
    static h2(children){
        return new ChatH2Element(children);
    }

    /**
     * Creates a h3 element. h elements are used as headings and range from h1 to h6 where the size of h1 is the biggest
     * and h6 is the smallest
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} A Child or an array of children.
     * @return {ChatH3Element}Returns the h3 for further processing
     * @example
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.h3("Hello World")
     * ]).renderAndSend();
     */
    static h3(children){
        return new ChatH3Element(children);
    }

    /**
     * Creates a h4 element. h elements are used as headings and range from h1 to h6 where the size of h1 is the biggest
     * and h6 is the smallest
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} A Child or an array of children.
     * @return {ChatH4Element}Returns the h4 for further processing
     * @example
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.h4("Hello World")
     * ]).renderAndSend();
     */
    static h4(children){
        return new ChatH4Element(children);
    }

    /**
     * Creates a h5 element. h elements are used as headings and range from h1 to h6 where the size of h1 is the biggest
     * and h6 is the smallest
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} A Child or an array of children.
     * @return {ChatH5Element}Returns the h5 for further processing
     * @example
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.h5("Hello World")
     * ]).renderAndSend();
     */
    static h5(children){
        return new ChatH5Element(children);
    }

    /**
     * Creates a h6 element. h elements are used as headings and range from h1 to h6 where the size of h1 is the biggest
     * and h6 is the smallest
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} A Child or an array of children.
     * @return {ChatH6Element}Returns the h6 for further processing
     * @example
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.h6("Hello World")
     * ]).renderAndSend();
     */
    static h6(children){
        return new ChatH6Element(children);
    }

    /**
     * Creates a new "custom" HTML Element, this may be useful, when using an HTML element for which no "shortcut" was defined by this library
     * @param element {string} The name of the HTML element, for example "div", "a", "b", etc.
     * @param canHaveChildren {boolean} Flag that determines if the element is allowed to have child elements
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} An optional single element or array of children.
     * @returns {ChatHtmlElement} Returns the created HTML Element for further processing
     * @example
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.a("https://www.example.com", "Hello World"), //Creates a "normal" link
     *      ChatMessageBuilder.a("https://www.example.com", ChatMessageBuilder.img("link/to/the/image/or/svg", 32, 32)) //Creates a clickable Image
     * ]).renderAndSend();
     */
    static html(element, canHaveChildren, children){
        return new ChatHtmlElement(element, canHaveChildren, children);
    }

    /**
     * Creates a new img Element, which can be used to include Images in the ChatMessage.
     * @param source {string} The source of the Image. Usually a relative URL to a resource on the server, web resources may be possible, but it will depend on the Image Server settings and the settings of Foundry
     * @param width {number?} Optional parameter which will set the width of the displayed image in pixels. If nether width nor height are set, the original size is used.
     * @param height {number?} Optional parameter which will set the height of the displayed image in pixels. If nether width nor height are set, the original size is used.
     * @param doNotUseCache {boolean?} Optional parameter which can be used to disable the Cache protection (if true, will insert a random parameter behind the url). If you do not know what this does, leaving it blank will work just fine. Default: true
     * @returns {ChatImgElement} Returns the Image element for further processing
     * @example
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.img("link/to/the/image/or/svg", 32, 32)
     * ]).renderAndSend();
     *
     */
    static img(source, width, height, doNotUseCache){
        return new ChatImgElement(source, width, height, doNotUseCache)
    }

    /**
     * Creates a new p element, which is often used to display text blocks.
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} An optional single element or array of children. In its base usage, a string is used
     * @returns {ChatPElement} Returns the p element for further processing
     * @example
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.p("This is a basic text block, it will wrap normally"), // Base usage
     *      ChatMessageBuilder.p(["This is the first string", " This second string will be appended"]), //Multiple strings
     *      ChatMessageBuilder.p(["This is a string", ChatMessageBuilder.a("https://www.example.com")]), //The p element allows further sub elements
     * ]).renderAndSend();
     */
    static p(children){
        return new ChatPElement(children);
    }

    /**
     * Creates a new span element, which represents a small line of text or part of it.
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} An Optional single element or array of children. Often only strings are used, but other "inline" elements like a, b, i, span, etc are also allowed
     * @returns {ChatSpanElement} Returns the span for further processing
     * @example
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.span("This is a basic text line"), // Base usage
     *      ChatMessageBuilder.span(["This is the first string", " This second string will be appended"]), //Multiple strings
     *      ChatMessageBuilder.span(["This is a string", ChatMessageBuilder.b(" This part will be bold")]), //The p element allows further sub elements
     * ]).renderAndSend();
     */
    static span(children){
        return new ChatSpanElement(children)
    }


    /*=================================================================================================
    * ================================ Predefined Elements ===========================================
    * =================================================================================================*/

    /**
     * Creates a container, in which content is displayed from top to bottom
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} An Optional single element or array of children.
     * @returns {ChatVerticalListElement} Returns the list for further processing
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.verticalList([
     *          "This is the first line",
     *          ChatMessageBuilder.p("This is the second line")
     *      ]);
     * ]).renderAndSend();
     */
    static verticalList(children){
        return new ChatVerticalListElement(children)
    }

    /**
     * Creates a container, in which content is displayed from left to right
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} An Optional single element or array of children.
     * @returns {ChatVerticalListElement} Returns the list for further processing
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.verticalList([
     *          "Left",
     *          ChatMessageBuilder.span("right")
     *      ]);
     * ]).renderAndSend();
     */
    static horizontalList(children){
        return new ChatVerticalListElement(children)
    }

    /**
     * Creates a header element, which includes a title and an optional image
     * @param title {string} The title which will be displayed
     * @param image {(boolean | string)?} An optional parameter. If true, the the character avatar is shown (or the character avatar, if no character is found). If false, no image is shown. Image source can be overwritten with a string.
     * @param imgLink {string?} An optional parameter. If defined, the image can be clicked opening the url which is specified in this parameter
     * @returns {ChatHeaderElement} Returns the header for further processing
     * @example
     *
     * ChatMessageBuilder.start().add([
     *      ChatMessageBuilder.header("Hello World"), // Will show an Avatar and the title
     *      ChatMessageBuilder.header("Hello World", false), //Will only show the title
     *      ChatMessageBuilder.header("Hello World", "link/to/img", "open/this/link/on/click"), //Shows a clickable image and a title
     * ]).renderAndSend();
     */
    static header(title, image = true, imgLink){
        return new ChatHeaderElement(title, image, imgLink)

    }

    /**
     * @private
     * Function that is used to test the presence of the Module. Creates a console log message
     */
    static testLog(){
        console.log("ChatMessageBuilder | Class Test Log");
    }

}
