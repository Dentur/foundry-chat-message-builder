/**
 * @external Roll
 * A [dice roll](https://foundryvtt.com/api/Roll.html) of the FoundryVtt API
 *
 */

/**
 * Definition of elements that can be used as child elements
 * @typedef {(ChatBaseElement | Roll | String)} ChatBaseElementChildren
 */

/**
 * Base building block of a chat message.
 * @class
 */
export class ChatBaseElement{

    /**
     * Array in which the children of this Element will be stored. May not accept children: Check {@link ChatBaseElement.canHaveChildren} to see if the element is allowed children.
     * @type {ChatBaseElementChildren[]}
     */
    children = [];

    /**
     * Parameter that can be used to indicate if this Element is allowed to have children. Useful for the {@link ChatBaseElement.render} Method and Documentation
     * @type {boolean}
     */
    canHaveChildren = true;


    /**
     * Creates a new instance of an ChatBaseElement without any children.
     * Can be useful to create a wrapper of multiple components without any HTML
     * @constructor
     */
    constructor() {

    }


    /**
     * Adds the Elements specified to the children of this Element.
     * @param elements {(ChatBaseElementChildren | ChatBaseElementChildren[])}
     * @returns {ChatBaseElement} Returns itself for further processing
     **/
    add(elements){
        if(!this.canHaveChildren)
            return this;
        if(Array.isArray(elements)){
            for(let element of elements){
                this.children.push(element);
            }
        }
        else{
            this.children.push(elements);
        }
        return this;
    }

    /**
     * Removes all children
     * @returns {ChatBaseElement} Returns itself for further processing
     */
    clear(){
        this.children = [];
        return this;
    }

    /**
     * Removes the specified element from the children
     * @param element {ChatBaseElementChildren} The child to remove
     * @returns {ChatBaseElement} Returns itself for further processing
     */
    remove(element){
        this.children = this.children.filter(child=>child !== element);
        return this;
    }

    //Note: If this Method is ever changed to not only return the string of the children, ChatHtmlElement.render() needs to be adjusted, as it uses this method.
    /**
     * Renders this element to a HTML String so it can be inserted in a [ChatMessage](https://foundryvtt.com/api/ChatMessage.html)
     * @returns {Promise<String>} An String containing the HTML representation of this element an all of its children
     */
    async render(){
        let returnValue = "";
        for(let child of this.children){
            if(child instanceof ChatBaseElement){
                returnValue+=`${await child.render()}\n`
            }
            else if (typeof child === "string"){
                returnValue+=`${child}\n`
            }
            else if (child instanceof Roll){
                returnValue+=`${await child.render()}\n`
            }
        }
        return returnValue;
    }



}
