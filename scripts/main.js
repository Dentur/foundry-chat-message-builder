import {ChatMessageBuilder} from "./ChatMessageBuilder.js";

Hooks.once("ChatMessageBuilder_loadTemplates", ()=>{
    return {
        templates: [],
        getDocumentation: ()=>{
            console.log("ChatMessageBuilder | getDocumentation WIP")
        },
        listExamples: ()=>{
            console.log("ChatMessageBuilder | listExamples WIP")
        }
    }
})


Hooks.once("ready", ()=>{
    console.log("ChatMessageBuilder | Initializing");
    //Register the builder
    window.ChatMessageBuilder = ChatMessageBuilder;

    //Load Templates
    Hooks.callAll("ChatMessageBuilder_loadTemplates", (a, b, c)=>{
        console.log("ChatMessageBuilder | ", a, b, c);
    })

    console.log("ChatMessageBuilder | Initialized");
})

Hooks.on("renderChatMessage", (message, jq, data)=>{
    //Set onClick callbacks
    jq.find(".ChatMessageBuilderButton").click(function(){
        console.log("ChatMessageBuilder | onClick")
        let callbackText = this.dataset.cmbOnclick
        if(!callbackText){
            console.warn("ChatMessageBuilder | Callback class was added, but no callback specified in data-CMB-onclick. Skipping!")
            return;
        }
        let callbackDataText = this.dataset.cmbOnclickData
        let callbackData = {}
        if(callbackDataText){
            try{
                callbackData = JSON.parse(callbackDataText)
            }
            catch(e){
                console.warn("ChatMessageBuilder | Could not parse callback Data")
            }
        }

        try{
            let callback = eval(callbackText)
            callback(this, callbackData)
        }
        catch(e){
            console.warn("ChatMessageBuilder | Could not parse or execute callback", e)
        }
    })
})




