/**
 * Created by Sebastian Venhuis on 25.04.2021.
 */
import {ChatHtmlElement} from "../html/ChatHtmlElement.js";

/**
 * An chat element which will display content vertically
 */
export class ChatVerticalListElement extends ChatHtmlElement{

    /**
     * Creates a new Vertical List element
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} Optional array of children of this element.
     */
    constructor(children) {
        super("div", true, children);
        this.addClass("chat-flex-vert")
    }
}
