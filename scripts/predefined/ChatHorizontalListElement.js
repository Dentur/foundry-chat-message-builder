/**
 * Created by Sebastian Venhuis on 25.04.2021.
 */
import {ChatHtmlElement} from "../html/ChatHtmlElement.js";

/**
 * An chat element which will display content horizontaly
 */
export class ChatHorizontalListElement extends ChatHtmlElement{

    /**
     * Creates a new horizontal list element
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} Optional array of children of this element.
     */
    constructor(children) {
        super("div", true, children);
        this.addClass("chat-flex-hor")
    }
}
