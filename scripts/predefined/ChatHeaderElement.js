/**
 * Created by Sebastian Venhuis on 25.04.2021.
 */
import {ChatHtmlElement} from "../html/ChatHtmlElement.js";
import {ChatHorizontalListElement} from "./ChatHorizontalListElement.js";
import {ChatH3Element} from "../html/ChatH3Element.js";
import {ChatAElement} from "../html/ChatAElement.js";
import {ChatImgElement} from "../html/ChatImgElement.js";

/**
 * Represents a Header element for the Chat Message. Contains an optional Image and a Text message
 */
export class ChatHeaderElement extends ChatHtmlElement{

    /**
     * Creates a new instance of the Header element
     * @param title {string} The text, that will be shown in the Header
     * @param image {(boolean | string)} Determines whether or not an image is shown to the left of the title. Defaults to the Owned Character Image and if not present, to User Image. Can be overwritten with a string for a custom image url
     * @param imgLink {string?} Only applicable, if image is true or a string. Will wrap the Image in a a Tag and use this parameter as a link
     */
    constructor(title, image = true, imgLink) {
        super("div", true);

        let children = [];

        if(image !== false){
            let src = "";
            if(image === true){
                if(game?.users?.current?.character?.data?.img){
                    src = game?.users?.current?.character?.data?.img
                }
                else{
                    src = game.users.current.avatar
                }
            }
            else
                src = image;

            let imgElement = new ChatImgElement(src, 32, 32);
            imgElement.addStyle({marginRight: "5px"})

            if(imgLink !== undefined){
                children.push(new ChatAElement(imgLink, imgElement));
            }
            else{
                children.push(imgElement)
            }

        }

        children.push(
            new ChatH3Element(title)
                .addStyle({
                    fontFamily: "\"Modesto Condensed\", serif",
                    margin: "0",
                    marginLeft: "5px",
                    alignSelf: "center",
                    fontSize: "20px",
                    fontWeight: "700",
                    color: "#4b4a44"
                })
        )

        this.add(new ChatHorizontalListElement(children))
    }
}
