/**
 * Created by Sebastian Venhuis on 17.05.2021.
 */
import {ChatHtmlElement} from "./ChatHtmlElement.js";

/**
 * Button which can execute code on click
 */
export class ChatButtonElement extends ChatHtmlElement {

    /**
     * Creates a new Button instance
     * @param callback {function(HTMLElement, Object)} The Callback which will be called, when the button is clicked.
     * The first parameter will be the HTMLElement of the Button, the second, the data specified in callbackData
     * <b>Note: As the callback may be executed by a different user (or device) than where the CharMessage were send
     * from, it needs to be fully independent. This manly means, that all data used by the callback needs to
     * be serialized and attached to the message, so that it can be stored on the server. Scoped variables will not Work!
     * Global variables will only work on the device, the chat message was generated on, and will not be persisted
     * between sessions </b>
     * @param callbackData {Object} An serializable object. This data will be
     * serialized and attached to the button. Once clicked, the data will be parsed and handed to the callback.
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} Optional array of children of this element.
     * @example
     * new ChatButtonElement((element)=>{console.log("element", element, "clicked"), "Click me!"}
     */
    constructor(callback, callbackData, children) {
        super("button", true, children);

        let serializedData = JSON.stringify(callbackData)

        this.addClass(["ChatMessageBuilderButton"])
        this.addAttribute("data-cmb-onclick-data", serializedData)
        this.addAttribute("data-cmb-onclick", callback.toString())
    }
}
