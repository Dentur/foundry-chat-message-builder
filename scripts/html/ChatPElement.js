/**
 * Created by Sebastian Venhuis on 23.04.2021.
 */
import {ChatHtmlElement} from "./ChatHtmlElement.js";

/**
 * Class that represents a p element for the ChatMessageBuilder. Shortcut class for the {@link  ChatHtmlElement} class
 */
export class ChatPElement extends ChatHtmlElement {
    /**
     * Creates a new p element for use with the ChatMessageBuilder
     * @param children {ChatBaseElementChildren[]?} Optional array of children of this element.
     * @example
     * let p = new ChatPElement(["Hello World"])
     */
    constructor(children) {
        super("p", true, children);
    }
}
