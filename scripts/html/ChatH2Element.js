/**
 * Created by Sebastian Venhuis on 23.04.2021.
 */
import {ChatHtmlElement} from "./ChatHtmlElement.js";

/**
 * Class that represents a h2 element for the ChatMessageBuilder. Shortcut class for the {@link  ChatHtmlElement} class
 */
export class ChatH2Element extends ChatHtmlElement {
    /**
     * Creates a new h2 element for use with the ChatMessageBuilder
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} Optional array of children of this element.
     * @example
     * let element = new ChatH2Element(["Hello World"])
     */
    constructor(children) {
        super("h2", true, children);
    }
}
