/**
 * Created by Sebastian Venhuis on 23.04.2021.
 */
import {ChatHtmlElement} from "./ChatHtmlElement.js";

/**
 * Class that represents a h1 element for the ChatMessageBuilder. Shortcut class for the {@link  ChatHtmlElement} class
 */
export class ChatH1Element extends ChatHtmlElement {
    /**
     * Creates a new h1 element for use with the ChatMessageBuilder
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} Optional array of children of this element.
     * @example
     * let element = new ChatH1Element(["Hello World"])
     */
    constructor(children) {
        super("h1", true, children);
    }
}
