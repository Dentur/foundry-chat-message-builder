/**
 * Created by Sebastian Venhuis on 23.04.2021.
 */
import {ChatHtmlElement} from "./ChatHtmlElement.js";

/**
 * Class that represents a "a" element for the ChatMessageBuilder. Shortcut class for the {@link  ChatHtmlElement} class
 */
export class ChatAElement extends ChatHtmlElement {

    /**
     * Creates a new a element to be used by the ChatMessageBuilder.
     * @param url {string?} The Url / Link / href this element will link to. Defaults to "#" (no action)
     * @param text {(ChatBaseElementChildren | ChatBaseElementChildren[])?} An Optional parameter that will set the children of the a element. Or the text
     * @example
     * let a = new ChatAElement("https://www.example.com", "Example Website");
     * let b = new ChatAElement("https://www.example.com", [new ChatPElement("Hello World")]);
     */
    constructor(url, text) {
        super("a", true);
        if(url)
            this.setHref(url);
        if(text)
            this.add(text)
    }

    /**
     * The Url / Link / href this element will link to. Defaults to "#" (no action)
     * @param url {string}
     * @returns {ChatAElement} Returns a instance of ChatAElement for further processing
     */
    setHref(url){
        this.addAttribute("href", url);
        return this;
    }
}
