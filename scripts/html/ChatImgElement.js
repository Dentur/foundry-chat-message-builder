/**
 * Created by Sebastian Venhuis on 23.04.2021.
 */
import {ChatHtmlElement} from "./ChatHtmlElement.js";

/**
 * Class that represents a "img" element for the ChatMessageBuilder. Shortcut class for the {@link  ChatHtmlElement} class
 * Note: By default, the source of the image will be followed by a ?<randomInt> to prevent image caching.
 */
export class ChatImgElement extends ChatHtmlElement {

    /**
     * Determines if a random parameter is added to the end of the source, to prevent browsers from caching
     * @type {boolean}
     */
    doNotCache = false;

    /**
     * Creates a new img element to be used by the ChatMessageBuilder.
     * @param src {string} The source of the image. Defaults to "icons/svg/mystery-man.svg"
     * @param width {number} The width parameter of the img element, if nether width nor height are specified, the Image will be displayed in original size.
     * @param height {number} The height parameter if the img element.  If nether width nor height are specified, the Image will be displayed in original size.
     * @param doNotCache {boolean} By default a random string is added to the image soruce to prevent the browser from caching the image. This can be disabled by setting this parameter to false
     */
    constructor(src="icons/svg/mystery-man.svg", width, height, doNotCache=true) {
        super("img", false);
        if(src)
            this.setSource(src);
        else
            console.warn(`ChatMessageBuilder | ChatImgElement: The source of the image was falsy, a default image will be used instead`)

        if(width !== undefined){
            this.addAttribute("width", width.toString());
        }
        if(height !== undefined){
            this.addAttribute("height", height.toString());
        }

        if(doNotCache === false){ //Note: !doNotCache is explicitly NOT used, so undefined (and other falsy values) are not counted
            this.doNotCache = doNotCache;
        }
    }

    /**
     * The source of the image. Defaults to "icons/svg/mystery-man.svg"
     * @param src {string} The Source Url
     * @returns {ChatImgElement} Returns a instance of itself for further processing
     */
    setSource(src){
        this.addAttribute("src", src+`?dummy=${Math.floor(Math.random()*80000+10000)}`);
        return this;
    }
}
