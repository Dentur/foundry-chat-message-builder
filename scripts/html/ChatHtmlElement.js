/**
 * Created by Sebastian Venhuis on 23.04.2021.
 */

import {ChatBaseElement} from "../ChatBaseElement.js";

/**
 * Chat Element, that represents a HTML element.
 */
export class ChatHtmlElement extends ChatBaseElement{
    /**
     * The htmlElement this object represents
     * @type {string}
     */
    htmlElement = "";


    /**
     * Creates a new instance of ChatHtmlElement representing on of the many possible html elements
     * A list of many html elements can be found at @see https://developer.mozilla.org/de/docs/Web/HTML/Element
     * Only Elements, that can be used in the Body should be used, no tags will be inserted in the header
     * @param htmlElement {string} The name of the html tag. This name will be used in between the < and >
     * @param canHaveChildren {boolean} This flag determines if this element can have children. Useful for tags which cannot have content
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} Optional array of children of this element. Will be ignored if canHaveChildren is false
     * @constructor
     * @example
     * let div = new ChatHtmlElement("div", true, ["This is a sample text", newChatHtmlElement("div", true), predefinedRoll]);
     * let input = new ChatHtmlElement("input", false);
     */
    constructor(htmlElement, canHaveChildren, children){
        super();
        this.htmlElement = htmlElement;
        this.canHaveChildren = canHaveChildren
        if(children){
            this.add(children);
        }
    }

    /**
     * The inline Style which will be applied to the html element
     * @type {Object.<string, string>}
     */
    style = {};

    /**
     * Sets and <b>Overwrites</b> the {@link style}. Accepts {@link CSSStyleDeclaration} or dictionary objects.
     * See the List of {@link https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference common CSS properties}
     * @param style {(Object.<string, (string|number)>)} The CSS elements to set. (hyphenCase)
     * @returns {ChatHtmlElement} Returns itself for further processing
     */
    setStyle(style){
        if (typeof style === "object"){
            this.style = {};
            this.addStyle(style);
        }
        else console.warn("ChatMessageBuilder | ChatHtmlElement.setStyle(style) only a Object.<string, (number | string)> dictionary")
        return this;
    }

    /**
     * Adds the Elements of the style dictionary to the current inline Style.
     * @param style {Object.<string,(string | number)>} The Css elements to set. See the List of[common CSS properties](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference) for an overview of Options.
     * @returns {ChatHtmlElement} Returns itself for further processing
     */
    addStyle(style){
        if(typeof style !== "object"){
            console.warn("ChatMessageBuilder | ChatHtmlElement.addStyle(style) style parameter not of type object. Skipping instruction.");
            return this;
        }
        for(let key in style){
            if(!style.hasOwnProperty(key)) continue;
            if(typeof style[key] === "string")
                this.style[key] = style[key];
            else
                this.style[key] = style[key].toString();
        }
        return this;
    }

    /**
     * Array of classes, that are applied to the chat message div
     * @type {string[]}
     */
    classes = []

    /**
     * Sets and <b>Overwrites</b> the{@link classes} array
     * @param classes {string[]} The new array of classes that will be applied to the chat message div
     * @returns {ChatHtmlElement} Returns itself for further processing
     */
    setClasses(classes){
        if(Array.isArray(classes)){
            this.classes = classes;
        }
        else{
            console.warn("ChatMessageBuilder | ChatHtmlElement.setClasses(classes) classes parameter was not an array. Skipping instruction");
        }
        return this;
    }

    /**
     * Adds the class or the classes to the list of {@link classes} that will be applied to the chat message div
     * @param classes {(string | string[])} String or String array of classes that will be added
     * @returns {ChatHtmlElement}Returns itself for further processing
     */
    addClass(classes){
        if(typeof classes === "string"){
            this.classes.push(classes);
        }
        else if (Array.isArray(classes)){
            for(let classString of classes){
                if(typeof classString === "string") {
                    this.classes.push(classString);
                }
                else {
                    console.warn(`ChatMessageBuilder | ChatHtmlElement.addClasses(classes) array element ${JSON.stringify(classString)} is no string. Skipping Element.`);
                }
            }
        }
        else{
            console.warn("ChatMessageBuilder | ChatHtmlElement.addClasses(classes) classes parameter is no string nor an array. Skipping instruction");
        }
        return this;
    }

    /**
     * An Dictionary of Attributes, that will be added to the Html Element
     * @type {Object.<string, string>}
     */
    attributes = {};

    /**
     * Adds (or overwrites if already present) the Attribute with the given name and value
     * @param name {string} The name of the attribute
     * @param value {string} The value of the attribute
     * @returns {ChatHtmlElement} Returns this for further processing
     */
    addAttribute(name, value){
        this.attributes[name] = value
        return this;
    }

    /**
     * Removes all set attributes
     * @returns {ChatHtmlElement} Returns this for further processing
     */
    clearAttributes(){
        this.attributes = {};
        return this;
    }

    /**
     * Renders this element and all of its children (if any)
     * @returns {Promise<string>} a HtmlString representation of this element
     */
    async render(){
        let containerNode = document.createElement("div");
        let htmlElement = document.createElement(this.htmlElement)
        containerNode.appendChild(htmlElement);
        for (let styleKey in this.style){
            if(!this.style.hasOwnProperty(styleKey)) continue;
            htmlElement.style[styleKey] = this.style[styleKey];
        }
        for(let classString of this.classes){
            htmlElement.classList.add(classString)
        }
        for(let attributeKey in this.attributes){
            if(this.attributes.hasOwnProperty(attributeKey)){
                htmlElement.setAttribute(attributeKey, this.attributes[attributeKey]);
            }
        }
        htmlElement.innerHTML = await super.render(); //Uses the render Method of the ChatBaseElement, which will only render the children
        return containerNode.innerHTML;
    }


}
