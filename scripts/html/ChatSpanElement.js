/**
 * Created by Sebastian Venhuis on 23.04.2021.
 */
import {ChatHtmlElement} from "./ChatHtmlElement.js";

/**
 * Class that represents a h1 element for the ChatMessageBuilder. Shortcut class for the {@link  ChatHtmlElement} class
 */
export class ChatSpanElement extends ChatHtmlElement {
    /**
     * Creates a new span element for use with the ChatMessageBuilder
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} Optional array of children of this element. Children of a span should be inline elements e.g. b, i, span, etc
     * @example
     * let span = new ChatSpanElement(["Hello World"])
     */
    constructor(children) {
        super("span", true, children);
    }
}
