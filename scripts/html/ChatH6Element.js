/**
 * Created by Sebastian Venhuis on 23.04.2021.
 */
import {ChatHtmlElement} from "./ChatHtmlElement.js";

/**
 * Class that represents a h6 element for the ChatMessageBuilder. Shortcut class for the {@link  ChatHtmlElement} class
 */
export class ChatH6Element extends ChatHtmlElement {
    /**
     * Creates a new h6 element for use with the ChatMessageBuilder
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} Optional array of children of this element.
     * @example
     * let element = new ChatH6Element(["Hello World"])
     */
    constructor(children) {
        super("h6", true, children);
    }
}
