/**
 * Created by Sebastian Venhuis on 23.04.2021.
 */
import {ChatHtmlElement} from "./ChatHtmlElement.js";

/**
 * Class that represents a div element for the ChatMessageBuilder. Shortcut class for the {@link  ChatHtmlElement} class
 */
export class ChatDivElement extends ChatHtmlElement {
    /**
     * Creates a new instance of the ChatDivElement.
     * @param children {(ChatBaseElementChildren | ChatBaseElementChildren[])?} Optional array of children of this element.
     */
    constructor(children) {
        super("div", true, children);
    }
}
