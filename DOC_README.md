# FoundryVTT Chat Message Builder Documentation

Welcome to the [FoundryVTT](https://foundryvtt.com/) Chat Message Builder Documentation.

## Links
Manifest: ``https://gitlab.com/Dentur/foundry-chat-message-builder/-/raw/master/module.json``

Gitlab: [foundry-chat-message-builder](https://gitlab.com/Dentur/foundry-chat-message-builder)

Issues: [Gitlab Issues](https://gitlab.com/Dentur/foundry-chat-message-builder/-/issues)


## How to install

> You may not want to install this Module at this state, as it is currently basically useless

1. Enter the Setup of FoundryVTT
2. Click the "Add-on Modules" Tab
3. Click the "Install Module" Button
4. Add this URL ``https://gitlab.com/Dentur/foundry-chat-message-builder/-/raw/master/module.json`` in the "Manifest 
   URL" text field
5. Click install
6. Enable this Module in the World(s) of your choosing

## Getting Started - The Builder
> Your use case may already be supported in form of a Template. Check out Getting Started - Templates for further
> information  

### TLDR
```javascript
let cmb = ChatMessageBuilder; //Less characters = less writing

let roll = new Roll("1d20"); //Create a roll
roll.evaluate();

cmb.start().add([
    cmb.span(cmb.b("Rocks fall")), 
    roll
]).renderAndSend(); //creats and sends the chat message
```

To get started, simply create a new Script Macro.

All content of this Module can be accessed over the [``ChatMessageBuilder``](./ChatMessageBuilder.html) global variable, which is set, 
once the Module is installed and activated.
It is highly recommended to create a variable with a short name to make use of the Library a lot quicker.
```javascript
let cmb = ChatMessageBuilder;
```

To create a Chat Message, first the [``ChatMessageBuilder.start()``](./ChatMessageBuilder.html#.start) method 
needs to be called. This will create a new instance of the [``ChatMessageBuilder``](./ChatMessageBuilder.html) class.

Usually the next step is to add content. Nearly all chat elements support the 
[``ChatBaseElement.add()``](./ChatBaseElement.html#add) method. It accepts either a single string, 
[``ChatBaseElement``](./ChatBaseElement.html), [``Roll``](https://foundryvtt.com/api/Roll.html) or an array containing
any combination of these.

At the end, the message needs to be rendered and send as a Chat Message. Usually the message can be send directly.
This can easily be archived by calling the 
[``ChatMessageBuilder.renderAndSend()``](./ChatMessageBuilder.html#renderAndSend) method. This will render the message
and internally call [``ChatMessage.create()``](https://foundryvtt.com/api/ChatMessage.html#.create). Alternatively, the
[``ChatMessageBuilder.render()``](./ChatMessageBuilder.html#render) Method may be used to receive a Promise<string> 
which will contain the string which can be used in the ChatMessage.

## Available Chat Elements
This Module provides a variety of elements with which a ChatMessage can be created. These have a fairly big range in 
abstraction, ranging from specialized Templates to low level html wrappers. In this section, the available elements will
be listed and shortly described

### Templates
Templates are your one liner to get a pretty chat message.
TODO

### Predefined Elements
Predefined elements are more complex elements which will create a specific output. The main difference to Templates is,
that these still need to be used in the ChatMessageBuilder.

- [``ChatMessageBuilder.verticalList``](./ChatMessageBuilder.html#.verticalList)
  - A simple list component in which elements will be displayed from top to bottom. Does not display list markers!
- [``ChatMessageBuilder.horizontalList``](./ChatMessageBuilder.html#.horizontalList)
   - A simple list component in which elements will be displayed from left to right. Does not display list markers!
- [``ChatMessageBuilder.header``](./ChatMessageBuilder.html#.horizontalList)
   - Inspired and designed to look like [BetterRollsFor5e](https://foundryvtt.com/packages/betterrolls5e) ([Github](https://github.com/RedReign/FoundryVTT-BetterRolls5e)). This element acts like a header for a Chat Messages. It includes a Title and optional an Image.

### Basic Elements
Basic elements are the low level, and can be used in many ways, which also makes them "harder" to use, as more code is 
needed.

 - [``ChatBaseElement``](./ChatBaseElement.html)
   - (internal) This Element is used as base for other elements. But a lot of functionality is contained in this element, especially 
     with respect to children (of an element).
 - [``ChatMessageBuilder.hmtl``](./ChatMessageBuilder.html#.html)
   - An element which can represent any HTML element. Many of the following elements use this element as their base. How 
     to add classes, attributes and styles is implemented here.
 - HTML Elements
    -  Note: The following elements can be accessed by calling ``ChatMessageBuilder.<elementName>`` where ``<elementName>`` 
       is replaced by the name of the element.
    - [``ChatMessageBuilder.a``](./ChatMessageBuilder.html#.a)
      - An element with which links or elements with links can be created.
    - [``ChatMessageBuilder.div``](./ChatMessageBuilder.html#.div)
      - An Element which can be used as a container for other elements.
    - [``ChatMessageBuilder.img``](./ChatMessageBuilder.html#.img)
      - An Element with which images can be created
    - [``ChatMessageBuilder.p``](./ChatMessageBuilder.html#.p)
      - An element which represents a paragraph 
    - [``ChatMessageBuilder.span``](./ChatMessageBuilder.html#.span)
      - An element which represents a line of text or a part of a line
    
