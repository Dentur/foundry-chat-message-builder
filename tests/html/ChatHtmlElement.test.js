/**
 * Created by Sebastian Venhuis on 25.04.2021.
 */

import "regenerator-runtime/runtime.js"
import {ChatHtmlElement} from "../../scripts/html/ChatHtmlElement.js";
import {ChatDivElement} from "../../scripts/html/ChatDivElement.js";

test("Basic html element", async ()=>{
    let el = new ChatHtmlElement("div", true);
    expect(await el.render()).toMatch(/<div.*>.*<\/div>/)
})

test("Text child", async ()=>{
    let el = new ChatHtmlElement("div", true, ["Hello World"]);
    expect(await el.render()).toMatch(/<div.*>Hello World(\n)?<\/div>/)
})

test("div child", async ()=>{
    let el = new ChatHtmlElement("div", true, [new ChatHtmlElement("div", true, "Hello World")]);
    expect(await el.render()).toMatch(/<div.*>.*<div.*>.*Hello World(\n)?<\/div>(\n)?<\/div>/)
})

test("Child with canHaveChilden=false", async ()=>{
    let el = new ChatHtmlElement("div", false, ["Hello World"]);
    expect(await el.render()).toMatch(/<div.*><\/div>/)
})

test("classes", async ()=>{
    let el = new ChatHtmlElement("div", true);
    expect(await el.addClass("will be deleted").setClasses([]).addClass("single").addClass(["multiple1", "multiple2"]).render()).toMatch(
        /<div.*class=\"single multiple1 multiple2\".*>.*<\/div>/
    )
})

test("addAttribute", async ()=>{
    let el = new ChatHtmlElement("div", true);
    expect(await el.addAttribute("attribute-a", "a").addAttribute("attribute-b", "b").render()).toMatch(
        /<div.*attribute-a=\"a\".*attribute-b=\"b\">.*<\/div>/
    )
})


test("addStyle", async ()=>{
    let el = new ChatHtmlElement("div", true);
    expect(await el.addStyle({fontSize: "20px"}).render()).toMatch(
        /<div.*style=\"font-size: 20px;\".*>.*<\/div>/
    )
})
