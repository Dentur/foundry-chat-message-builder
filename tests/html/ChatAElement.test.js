/**
 * Created by Sebastian Venhuis on 24.04.2021.
 */

import "regenerator-runtime/runtime.js"
import {ChatAElement} from "../../scripts/html/ChatAElement.js";

test("Empty Render", async ()=>{
    let a = new ChatAElement();
    let a_render = await a.render();
    expect(a_render).toMatch(/<a.*>.*<\/a>/)
})

test("href set", async ()=>{
    let a = new ChatAElement("http://www.example.org");
    expect(await a.render()).toMatch(/<a.*href=\"http:\/\/www\.example\.org\".*>.*<\/a>/)
})

test("url set", async ()=>{
    let a = new ChatAElement(undefined, "Test Name");
    expect(await a.render()).toMatch(/<a.*>Test Name(\n)*<\/a>/)
})
