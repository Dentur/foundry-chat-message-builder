/**
 * Created by Sebastian Venhuis on 25.04.2021.
 */



import "regenerator-runtime/runtime.js"
import {ChatImgElement} from "../../scripts/html/ChatImgElement.js";

test("basic", async ()=>{
    let el = new ChatImgElement();
    expect(await el.render()).toMatch(/<img.*>/)
})
