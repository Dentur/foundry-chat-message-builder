/**
 * Created by Sebastian Venhuis on 18.05.2021.
 */

import "regenerator-runtime/runtime.js"
import {ChatButtonElement} from "../../scripts/html/ChatButtonElement.js";

test("SimpleButton", async ()=>{
    let button = new ChatButtonElement((button, data)=>{
        console.log("test")
    }, {test: true}, "Click Me")
    expect(await button.render()).toMatch(
        /<button.*onclick=\".*\".*data-CMB-onclick=\".*\">.*Click Me.*<\/button>/
    )
})
