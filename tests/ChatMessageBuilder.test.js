/**
 * Created by Sebastian Venhuis on 25.04.2021.
 */
import "regenerator-runtime/runtime.js"
import {ChatMessageBuilder} from "../scripts/ChatMessageBuilder.js";


test("Empty Chat Message", async ()=>{
    let message = await ChatMessageBuilder.start().render();
    expect(message).toMatch(/<div.*class=\".*chat-card.*\".*>.*<\/div>/)
})
