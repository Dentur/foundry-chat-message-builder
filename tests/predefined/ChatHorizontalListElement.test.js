/**
 * Created by Sebastian Venhuis on 25.04.2021.
 */

import "regenerator-runtime/runtime.js"
import {ChatHorizontalListElement} from "../../scripts/predefined/ChatHorizontalListElement.js";

test("Empty", async ()=>{
    let element = new ChatHorizontalListElement();
    expect(await element.render()).toMatch(/<div.*class=\".*chat-flex-hor.*\".*>.*<\/div>/);
})
