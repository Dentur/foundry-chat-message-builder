/**
 * Created by Sebastian Venhuis on 25.04.2021.
 */

import "regenerator-runtime/runtime.js"
import {ChatHeaderElement} from "../../scripts/predefined/ChatHeaderElement.js";

test("title only", async ()=>{
    let el = new ChatHeaderElement("title", false);
    console.log(await el.render());
    expect(await el.render()).toMatch(
        /<div.*><div.*><h3.*>title(\n)?<\/h3>(\n)?<\/div>(\n)?<\/div>/
    )
})

test("title + image", async ()=>{
    let el = new ChatHeaderElement("title", "customSrc");
    expect(await el.render()).toMatch(
        /<div.*><div.*><img.*src=\"customSrc\?dummy=[0-9]*\".*>(\n)?<h3.*>.*title(\n)?<\/h3>(\n)?<\/div>(\n)?<\/div>/
    )
})

test("title + image + link", async ()=>{
    let el = new ChatHeaderElement("title", "customSrc", "customLink");
    expect(await el.render()).toMatch(
        /<div.*><div.*><a.*href=\"customLink\".*><img.*src=\"customSrc\?dummy=[0-9]*\".*>(\n)?<\/a>(\n)?<h3.*>.*title(\n)?<\/h3>(\n)?<\/div>(\n)?<\/div>/
    )
})
